**New Orleans resident recreation and life**

Wellness isn't just about physicality. 
We recognize that Resident Recreation and Life in New Orleans leisure and life and socialization are vital to everyone's 
sense of well-being, as loneliness can diminish the immune system and have other harmful effects on physical and emotional health.
Please Visit Our Website [New Orleans resident recreation and life](https://youtu.be/0ocf7u76WSo) 
for more information. 
---

## New Orleans resident recreation and life

New Orleans Life Enrichment and Resident Leisure and Life Opportunities 
They are designed for relaxation in all aspects of health and wellness: social, 
emotional, academic, spiritual, occupational (sense of purpose) and environmental. They support our programming of physical wellness.